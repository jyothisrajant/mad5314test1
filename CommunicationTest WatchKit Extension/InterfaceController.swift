//
//  InterfaceController.swift
//  CommunicationTest WatchKit Extension
//
//  Created by Parrot on 2019-10-26.
//  Copyright © 2019 Parrot. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

class InterfaceController: WKInterfaceController, WCSessionDelegate {

    
    // MARK: Outlets
    // ---------------------
    @IBOutlet var messageLabel: WKInterfaceLabel!
   
    // Imageview for the pokemon
    @IBOutlet var pokemonImageView: WKInterfaceImage!
    // Label for Pokemon name (Albert is hungry)
    @IBOutlet var nameLabel: WKInterfaceLabel!
    // Label for other messages (HP:100, Hunger:0)
    @IBOutlet var outputLabel: WKInterfaceLabel!
    
    var gameTimer: Timer?
    var hungerLevel=0
    var healthLevel=100
    var hibernationMode=false
    var deadStatus=false
    var timerStatus=false
    var babyType=""
    var babyName="Baby"
    var babystate=""
    
    
    
    // MARK: Delegate functions
    // ---------------------

    // Default function, required by the WCSessionDelegate on the Watch side
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        //@TODO
    }
    
    // 3. Get messages from PHONE
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        print("WATCH: Got message from Phone")
        // Message from phone comes in this format: ["course":"MADT"]
//        let messageBody = message["course"] as! String
//        messageLabel.setText(messageBody)
        let babyName = message["babyname"] as! String
      let babywake=message["babywake"] as!String
        
        if(babywake=="true"){
            hibernationMode=false
        }
        
        if(babyName=="pokemon"){
            babyType="pokemon"
            self.pokemonImageView.setImageNamed("pikachu")
            nameLabel.setText("Hai,Happy Life !")
            
        }else if (babyName=="caterpie"){
            babyType="caterpie"
            self.pokemonImageView.setImageNamed("caterpie")
        }
        timerStatus=true
        outputLabel.setText("HP= 100 Hunger= 0")
        
    }
    


    
    // MARK: WatchKit Interface Controller Functions
    // ----------------------------------
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        
        // 1. Check if teh watch supports sessions
        if WCSession.isSupported() {
            WCSession.default.delegate = self
            WCSession.default.activate()
        }
        
        
        
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
           gameTimer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(updateGame), userInfo: nil, repeats: true)
        

        
        
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    
    // MARK: Actions
    // ---------------------
    
    // 2. When person presses button on watch, send a message to the phone
    @IBAction func buttonPressed() {
        
        if WCSession.default.isReachable {
            print("Attempting to send message to phone")
            self.messageLabel.setText("Sending msg to watch")
            WCSession.default.sendMessage(
                ["name" : "Pritesh"],
                replyHandler: {
                    (_ replyMessage: [String: Any]) in
                    // @TODO: Put some stuff in here to handle any responses from the PHONE
                    print("Message sent, put something here if u are expecting a reply from the phone")
                    self.messageLabel.setText("Got reply from phone")
            }, errorHandler: { (error) in
                //@TODO: What do if you get an error
                print("Error while sending message: \(error)")
                self.messageLabel.setText("Error sending message")
            })
        }
        else {
            print("Phone is not reachable")
            self.messageLabel.setText("Cannot reach phone")
        }
    }
    
    
    // MARK: Functions for Pokemon Parenting
    @IBAction func nameButtonPressed() {
        print("name button pressed")
    }

    @IBAction func startButtonPressed() {
        print("Start button pressed")
        if(deadStatus){
            
            print("RIP baby")
            
        }else{
       // hibernationMode=false
        }
    }
    
    @IBAction func feedButtonPressed() {
        print("Feed button pressed")
        if(deadStatus){
            
            print("RIP baby")
            
        }else{
            nameLabel.setText("Thank you for feeding me")
        hungerLevel=hungerLevel-12
             outputLabel.setText("HP= "+"\(healthLevel)"+" Hunger= "+"\(hungerLevel)")
        }
    }
    
    @IBAction func hibernateButtonPressed() {
        print("Hibernate button pressed")
        
        if(deadStatus){
            
            print("RIP baby")
            
        }else{
        hibernationMode=true
            
            if WCSession.default.isReachable {
                print("Attempting to send message to phone")
                self.messageLabel.setText("Sending msg to watch")
                WCSession.default.sendMessage(
                    ["name" : "\(babyName)","life":"\(healthLevel)","hunger":"\(hungerLevel)","type":"\(babyType)","state":"hibernate"],
                    replyHandler: {
                        (_ replyMessage: [String: Any]) in
                        // @TODO: Put some stuff in here to handle any responses from the PHONE
                        print("Message sent, put something here if u are expecting a reply from the phone")
                        self.messageLabel.setText("Got reply from phone")
                }, errorHandler: { (error) in
                    //@TODO: What do if you get an error
                    print("Error while sending message: \(error)")
                    self.messageLabel.setText("Error sending message")
                })
            }
            else {
                print("Phone is not reachable")
                self.messageLabel.setText("Cannot reach phone")
            }
            
        }
    }
    
   
    
    
    @objc func updateGame(){
        if(timerStatus){
        
        if(deadStatus){
        
        print("RIP baby")
        
        }else{
            if(hibernationMode){
                print("Hibermation Mode is active")
            }else{
                if(healthLevel==0){
                    
                    deadStatus=true
                    nameLabel.setText("RIP !")
                    
                    print("RIP baby")
                    
                }else{
                    hungerLevel=hungerLevel+10
                    if(hungerLevel>=80){
                        nameLabel.setText("I am soo Hungry")
                        healthLevel=healthLevel-5
                    }
                    outputLabel.setText("HP= "+"\(healthLevel)"+" Hunger= "+"\(hungerLevel)")
                }}
            
            
            }}}
    
    
}
