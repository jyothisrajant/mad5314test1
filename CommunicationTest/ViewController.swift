//
//  ViewController.swift
//  CommunicationTest
//
//  Created by Parrot on 2019-10-26.
//  Copyright © 2019 Parrot. All rights reserved.
//

import UIKit
import WatchConnectivity

class ViewController: UIViewController, WCSessionDelegate  {

    // MARK: Outlets
    @IBOutlet weak var outputLabel: UITextView!
    @IBOutlet weak var ganeStatusLabel: UILabel!
    @IBOutlet weak var wakeUpBtn: UIButton!
    @IBOutlet weak var gameImage: UIImageView!
    @IBOutlet weak var gameStatusTitle: UILabel!
    @IBOutlet weak var chooseLabel: UILabel!
    @IBOutlet weak var picImage: UIButton!
    @IBOutlet weak var topGroup: UIButton!
    @IBOutlet weak var catImage: UIButton!
    
    // MARK: Required WCSessionDelegate variables
    
    
    var babyname=""
    var babyType=""
    var lifeLevel=""
    var hungerLevel=""
    var babyState=""
    // ------------------------------------------
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        //@TODO
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        //@TODO
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        //@TODO
    }
    
    // MARK: Receive messages from Watch
    // -----------------------------------
    
    // 3. This function is called when Phone receives message from Watch
    func session(_ session: WCSession, didReceiveMessage message: [String : Any], replyHandler: @escaping ([String : Any]) -> Void) {
        self.babyname = message["name"] as! String
        self.babyType = message["type"] as! String
        self.babyState = message["state"] as! String
        self.lifeLevel = message["life"] as! String
        self.hungerLevel = message["hunger"] as! String
        
        // 1. When a message is received from the watch, output a message to the UI
        // NOTE: Since session() runs in background, you cannot directly update UI from the background thread.
        // Therefore, you need to wrap any UI updates inside a DispatchQueue for it to work properly.
        DispatchQueue.main.async {
           
            
            //self.outputLabel.insertText("\nMessage Received: \(message)")
            self.wakeUpBtn.isHidden=false
            self.ganeStatusLabel.isHidden=false
            self.gameImage.isHidden=false
            self.gameStatusTitle.isHidden=false
            self.picImage.isHidden=true
            self.catImage.isHidden=true
            self.chooseLabel.isHidden=true
            self.ganeStatusLabel.text="Your baby is hibernating.."
            self.gameStatusTitle.text="Game Status"
            if(self.babyType=="pokemon"){
                self.gameImage.image=UIImage(named: "pikachu")
            }else{
                self.gameImage.image=UIImage(named: "caterpie")
            }
            self.storeData()
            
            
            
            
        }
        
        // 2. Also, print a debug message to the phone console
        // To make the debug message appear, see Moodle instructions
        print("Received a message from the watch: \(message)")
    }

    
    // MARK: Default ViewController functions
    // -----------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // 1. Check if phone supports WCSessions
        print("view loaded")
        
        self.wakeUpBtn.isHidden=true
        self.ganeStatusLabel.isHidden=false
        self.gameImage.isHidden=true
         self.gameStatusTitle.isHidden=true
        ganeStatusLabel.text="Start the game.."
        if WCSession.isSupported() {
            outputLabel.insertText("\nPhone supports WCSession")
            WCSession.default.delegate = self
            WCSession.default.activate()
            outputLabel.insertText("\nSession activated")
        }
        else {
            print("Phone does not support WCSession")
            outputLabel.insertText("\nPhone does not support WCSession")
        }
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    // MARK: Actions
    // -------------------
    @IBAction func sendMessageButtonPressed(_ sender: Any) {
        
        // 2. When person presses button, send message to watch
        outputLabel.insertText("\nTrying to send message to watch")
        
        // 1. Try to send a message to the phone
        if (WCSession.default.isReachable) {
            let message = ["course": "MADT"]
            WCSession.default.sendMessage(message, replyHandler: nil)
            // output a debug message to the UI
            outputLabel.insertText("\nMessage sent to watch")
            // output a debug message to the console
            print("Message sent to watch")
        }
        else {
            print("PHONE: Cannot reach watch")
            outputLabel.insertText("\nCannot reach watch")
        }
    }
    
    
    // MARK: Choose a Pokemon actions
    
    @IBAction func pokemonButtonPressed(_ sender: Any) {
       ganeStatusLabel.text="Take care of your baby.."
        // 1. Try to send a message to the phone
         gameStatusTitle.text="Take care of your baby.."
        if (WCSession.default.isReachable) {
            let message = ["babyname": "pokemon","babywake": "nil"]
            WCSession.default.sendMessage(message, replyHandler: nil)
            // output a debug message to the UI
            outputLabel.insertText("\nMessage sent to watch")
            // output a debug message to the console
            print("Message sent to watch")
        }
        else {
            print("PHONE: Cannot reach watch")
            outputLabel.insertText("\nCannot reach watch")
        }
    }
    @IBAction func caterpieButtonPressed(_ sender: Any) {
        ganeStatusLabel.text="Take care of your baby.."
        print("You pressed the caterpie button")
        // 1. Try to send a message to the phone
        if (WCSession.default.isReachable) {
            let message = ["babyname": "caterpie","babywake": "nil"]
            WCSession.default.sendMessage(message, replyHandler: nil)
            // output a debug message to the UI
            outputLabel.insertText("\nMessage sent to watch")
            // output a debug message to the console
            print("Message sent to watch")
        }
        else {
            print("PHONE: Cannot reach watch")
            outputLabel.insertText("\nCannot reach watch")
        }
    }
    
    @IBAction func wakeUpBtnAction(_ sender: Any) {
        print("You pressed the wakeup button")
        // 1. Try to send a message to the phone
        if (WCSession.default.isReachable) {
            let message = ["babywake": "true","babyname": "nil"]
            WCSession.default.sendMessage(message, replyHandler: nil)
            // output a debug message to the UI
            outputLabel.insertText("\nMessage sent to watch")
            // output a debug message to the console
            print("Message sent to watch")
        }
        else {
            print("PHONE: Cannot reach watch")
            outputLabel.insertText("\nCannot reach watch")
        }
        self.wakeUpBtn.isHidden=true
        self.gameImage.isHidden=true
       self.ganeStatusLabel.text="No one is hibernating.."
    }
    
    //Shared Prefereces
    func storeData(){
        let sharedPref = UserDefaults.standard
        sharedPref.setValue(""+babyname, forKey: "BabyName")
        sharedPref.setValue(""+lifeLevel, forKey: "BabyLife")
        sharedPref.setValue(""+babyType, forKey: "BabyType")
        sharedPref.setValue(""+hungerLevel, forKey: "BabyHunger")
    }
    
}

